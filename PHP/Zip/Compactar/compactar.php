<?php

//// Instancia a Classe Zip
//$zip = new ZipArchive();
//
//// Cria o Arquivo Zip, caso não consiga exibe mensagem de erro e finaliza script
//$zip->open( 'nome_arquivo_zip.zip', ZIPARCHIVE::CREATE );
//
//// Insere os arquivos que devem conter no arquivo zip
//$zip->addFile( 'file_1.txt', 'file_1.txt' );
//$zip->addFile( 'file_1.txt', 'file_2.txt' );
//
//// Fecha arquivo Zip aberto
//$zip->close();

// Cria uma pasta compactada
$zip = new ZipArchive();
$zip->open( 'dir_compactados/nome_do_arquivo.zip', ZipArchive::CREATE );

// Adiciona um arquivo à pasta
$zip->addFile( 'file_1.txt', 'file_1.txt' );
$zip->addFile( 'dir_compactar', 'dir_compactar' );
$zip->addFile( 'file_2.txt', 'file_2.txt' );

// Fecha a pasta e salva o arquivo
$zip->close();
