<!DOCTYPE html>
<html lang=en>
<head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name=robots content=noindex>
    <title>WSE CRIAR TVSTATION on Vimeo</title>
    <style>body, html {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        #crawler_player {
            position: relative;
            width: 100%;
            height: 100%;
            background: url('https://i.vimeocdn.com/video/485854038_640.jpg') 50% 50% no-repeat;
            background-size: cover
        }

        #crawler_player button {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -53px;
            margin-top: -33px;
            width: 105px;
            height: 65px;
            background: url('https://f.vimeocdn.com/p/images/crawler_play.png');
            border: 0;
            text-indent: -1000em;
            -moz-opacity: .85;
            -webkit-opacity: .85;
            opacity: .85
        }

        #crawler_player .logo {
            position: absolute;
            bottom: 12px;
            right: 10px
        }

        #crawler_player a {
            display: none
        }</style>
<body>
<a href="https://vimeo.com/103568006">
    <div id=crawler_player>
        <button>Play</button>
        <img class=logo src="https://f.vimeocdn.com/p/images/crawler_logo.png" alt=Vimeo></div>
</a>
</body>
</html>