<?php

class WelcomeTest extends PHPUnit_Framework_TestCase
{

    private $CI;

    public function setUp()
    {
        $this->CI = &get_instance();
    }

    public function testClassExists()
    {
        $class = 'Welcome';
        $this->assertTrue( class_exists( $class ), 'Class not found: ' . $class );
    }

    public function testMethodIndex()
    {
        $this->assertTrue( true, 'welcome/index' );
    }

}
 