<?php

namespace src\Admin\Model;

use config\Model;

class Usuario extends Model
{

    public function listar()
    {
        return $this->setTabela( 'login' )->findAll();
    }
}