<?php

namespace config;

use PDO;

class Model
{

    private $tabela;

    public function findAll()
    {
        $sql = 'SELECT * FROM ' . $this->getTabela();
        $query = Connection::db()->prepare( $sql );
        $query->execute();
        return $query->fetchAll( PDO::FETCH_ASSOC );
    }

    /**
     * @return mixed
     */
    public function getTabela()
    {
        return $this->tabela;
    }

    /**
     * @param mixed $tabela
     */
    public function setTabela( $tabela )
    {
        $this->tabela = $tabela;
        return $this;
    }


}