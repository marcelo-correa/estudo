<?php

namespace config;

use PDO;

class Connection
{
    private static $conn;
    private static $hostname = 'localhost';
    private static $username = 'root';
    private static $database = 'pippi';
    private static $password = '';

    public static function db()
    {
        if ( is_null( self::$conn ) )
            self::$conn = new \PDO( 'mysql:host=' . self::$hostname . ';dbname=' . self::$database, self::$username, self::$password );

        return self::$conn;
    }
}