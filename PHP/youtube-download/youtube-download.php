<?php

// PARÂMETROS -------------------------------------------------------

// string de busca
$busca = 'php development';

// autor (canal) dos vídeos [opcional]
$author = '';

// filtrar o título pelas palavras [opcional]
// se o vídeo não possuir uma dessas palavras no título, é ignorado
$filtrarTitulo = array( 'PHP', 'screencast' );

// ------------------------------------------------------------------


// URL base para da API
$baseUrl = "http://gdata.youtube.com/feeds/api/videos";

// Parâmetros
$params = array(

    // string de busca
    'q'           => $busca,

    // ordenação
    'orderby'     => 'published',

    // registros retornados
    'start-index' => 1,
    'max-results' => 10,

    // resultado indentado
    'prettyprint' => 'false',

    // versão da API
    'v'           => 2,
);

// adiciona o autor nos parâmetros
if ( isset( $author ) && !empty( $author ) ) $params['author'] = $author;

// monta a string que será usada na expressão regular para filtrar pelo título
if ( isset( $filtrarTitulo ) && count( $filtrarTitulo ) > 0 )
    $filterGroup = implode( '|', $filtrarTitulo );
else
    $filterGroup = null;

// monta a url da requisição com os parâmetros
$url = $baseUrl . '?' . http_build_query( $params );

while ( $url ) {
    // consulta a API e lê o XML retornado
    $content = file_get_contents( $url ) or die( "ERRO: Não foi possível consultar os vídeos\n" );
    $feed = simplexml_load_string( $content ) or die( "ERRO: Não foi possível ler o XML\n" );

    foreach ( $feed->entry as $entry ) {
        $title = (string) $entry->title;

        // filtra os vídeos pelo título
        if ( $filterGroup && !preg_match( "/($filterGroup)/i", $title ) ) continue;

        echo "$title\n";

        // pega o id do vídeo
        if ( preg_match( "/video:([^:]+)/", (string) $entry->id, $m ) )
            $id = $m[1];
        else
            continue;

        $videoUrl = "http://www.youtube.com/watch?v=" . urlencode( $id );

        // executa o comando no shell
        $command = 'youtube-dl -t ' . escapeshellarg( $videoUrl );
        //echo "$command\n";
        passthru( $command );
    }

    $url = false;

    // pega a url da próxima página, se houver
    foreach ( $feed->link as $link ) {
        if ( $link['rel'] == 'next' ) {
            $url = $link['href'];
        }
    }
}

