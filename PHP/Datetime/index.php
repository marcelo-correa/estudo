<?php

//$date = 'Tue, Aug 4 2015 04:23:03 +0000';
$date = 'mar, ago 4 2015 00:04:53 +0200';

echo convertItalyToUSA( $date );

function convertItalyToUSA( $date )
{
    $dataCompleta = strtolower( $date );
    $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ"!@#$%&*()={[}]/?;.\\\'<>°ºª';
    $b = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                             ';
    $dataCompleta = utf8_decode( $dataCompleta );
    $dataCompleta = strtr( $dataCompleta, utf8_decode( $a ), $b );
    $dataCompleta = strip_tags( trim( $dataCompleta ) );
    $dataCompleta = str_replace( [ "-----", "----", "---", "--" ], "", $dataCompleta );
    $dataCompleta = strtolower( utf8_encode( $dataCompleta ) );

    $diaSemana = substr( $dataCompleta, 0, 3 );
    $mes = substr( $dataCompleta, 5, 3 );

    $arrayDiaSemanaIdioma = [ 'dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sab', 'lun', 'mar', 'mie', 'jue', 'vie', 'mer', 'gio', 'ven' ];
    $arrayDiaSemanaIngles = [ 'sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'mon', 'tue', 'wed', 'thu', 'fri', 'wed', 'thu', 'fri' ];

    $arrayMesesIdioma = [ 'jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez', 'ene', 'feb', 'may', 'sep', 'oct', 'dic', 'gen', 'apr', 'mag', 'giu', 'lug', 'ott' ];
    $arrayMesesIngles = [ 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'set', 'oct', 'nov', 'dec', 'jan', 'feb', 'may', 'set', 'oct', 'dec', 'jan', 'apr', 'may', 'jun', 'jul', 'oct' ];

    $TotalCaracteres = strlen( $dataCompleta );
    $CaracteresRestantes = $TotalCaracteres - 8;

    $parteFinal = substr( $dataCompleta, 8, $CaracteresRestantes );

    $alterarDiaSemana = str_replace( $arrayDiaSemanaIdioma, $arrayDiaSemanaIngles, $diaSemana ) . ', ' . str_replace( $arrayMesesIdioma, $arrayMesesIngles, $mes ) . $parteFinal;

    return $alterarDiaSemana;
}
echo '<br />';
echo ( new DateTime( convertItalyToUSA( $date ) ) )->format( 'd/m/Y' );