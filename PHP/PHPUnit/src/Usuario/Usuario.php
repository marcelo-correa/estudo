<?php


namespace Usuario;


class Usuario
{

    private $nome;

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome( $nome )
    {
        $this->nome = $nome;
    }


    public function findAll()
    {
        return array(
            'id'   => 10,
            'nome' => 'Marcelo'
        );
    }

} 