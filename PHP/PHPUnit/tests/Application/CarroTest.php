<?php

namespace tests\Application;

use Application\Carro;

class CarroTest extends \PHPUnit_Framework_TestCase
{

    public function assertPreConditions()
    {
        $this->assertTrue(
            class_exists( $classe = 'Application\Carro' ),
            'Class not found: ' . $classe
        );
    }

    public function testGetCor()
    {
        $Carro = new Carro();
        $Carro->setCor( "Azul" );

        $this->assertEquals( "Azul", $Carro->getCor() );

    }

    public function testInstantiation()
    {
        $Carro = new Carro();
        $this->assertInstanceOf( 'Application\Carro', $Carro );
    }
}
 