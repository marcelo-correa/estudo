<?php

require 'vendor/autoload.php';

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'mail.mcgestor.com.br';  // Specify main and backup SMTP servers
$mail->SMTPAuth = TRUE;                               // Enable SMTP authentication
$mail->Username = 'contato@mcgestor.com.br';                 // SMTP username
$mail->Password = 'Marsc2014';                           // SMTP password
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 465;                                    // TCP port to connect to

$mail->From = 'contato@mcgestor.com.br';
$mail->FromName = 'Mailer';
$mail->addAddress( 'marcelocorrea229@gmail.com', 'Marcelo' );     // Add a recipient
$mail->addAddress( 'mcgestor.com.br' );               // Name is optional
//$mail->addReplyTo( 'info@example.com', 'Information' );
//$mail->addCC( 'cc@example.com' );
//$mail->addBCC( 'bcc@example.com' );

//$mail->addAttachment( '/var/tmp/file.tar.gz' );         // Add attachments
//$mail->addAttachment( '/tmp/image.jpg', 'new.jpg' );    // Optional name
$mail->isHTML( TRUE );                                  // Set email format to HTML

$mail->Subject = 'Here is the subject';
$mail->Body = 'This is the HTML message body <b>in bold!</b>';
$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

//Envia o e-mail
$enviado = $mail->Send();

// Limpa os destinatários e os anexos
$mail->ClearAllRecipients();
$mail->ClearAttachments();

// Exibe uma mensagem de resultado
if ( $enviado ) {
    echo "E-mail enviado com sucesso!";
} else {
    echo "Não foi possível enviar o e-mail.";
    echo "<b>Informações do erro:</b> ";
    echo '<pre>';
    print_r( $mail->ErrorInfo );
    echo '</pre>';
    exit();
}