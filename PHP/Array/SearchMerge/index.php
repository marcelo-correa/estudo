<?php

$array_a = [
    [ 'cupertinostreamingpacketizer', 0 ],
    [ 'smoothstreamingpacketizer', 1 ],
    [ 'sanjosestreamingpacketizer', 2 ],
    [ 'mpegdashstreamingpacketizer', 3 ]
];

$array_b = [
    [ 'mpegdashstreamingpacketizer', 1 ],
    [ 'smoothstreamingpacketizer', 3 ]
];

echo '<pre>';
print_r( $array_a );
echo '</pre>';
echo '<pre>';
print_r( $array_b );
echo '</pre>';
echo '<pre>';
echo '////////////////////////////////////';
echo '</pre>';

foreach ( $array_b as $name ) {
    unset( $array_a[searchForId( $name[0], $array_a )] );
}

echo '<pre>';
print_r( array_merge( $array_a, $array_b ) );
echo '</pre>';
exit();

function searchForId( $id, $array )
{
    foreach ( $array as $key => $val ) {
        if ( $val[0] === $id ) {
            return $key;
        }
    }

    return NULL;
}

