<?php

class RestApiWowza
{

    private $username;
    private $password;
    private $method_request;
    private $user_agent = 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)';
    private $https = 'http://';
    private $host = '192.168.1.150';
    private $port = '8087';
    private $versao_api = 'v2';
    private $server = '_defaultServer_';
    private $vhost = '_defaultVHost_';
    private $URL;
    private $format_return = 'application/json';

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername( $username )
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword( $password )
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMethodRequest()
    {
        return $this->method_request;
    }

    /**
     * @param mixed $method_request
     */
    public function setMethodRequest( $method_request )
    {
        $this->method_request = $method_request;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->user_agent;
    }

    /**
     * @param string $user_agent
     */
    public function setUserAgent( $user_agent )
    {
        $this->user_agent = $user_agent;
        return $this;
    }

    /**
     * @return string
     */
    public function getHttps()
    {
        return $this->https;
    }

    /**
     * @param string $https
     */
    public function setHttps( $https )
    {
        $this->https = $https ? 'https://' : 'http://';
    }


    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost( $host )
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $port
     */
    public function setPort( $port )
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return string
     */
    public function getVersaoApi()
    {
        return $this->versao_api;
    }

    /**
     * @param string $versao_api
     */
    public function setVersaoApi( $versao_api )
    {
        $this->versao_api = $versao_api;
        return $this;
    }

    /**
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param string $server
     */
    public function setServer( $server )
    {
        $this->server = $server;
        return $this;
    }

    /**
     * @return string
     */
    public function getVhost()
    {
        return $this->vhost;
    }

    /**
     * @param string $vhost
     */
    public function setVhost( $vhost )
    {
        $this->vhost = $vhost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getURL()
    {
        return $this->URL;
    }

    /**
     * @param mixed $URL
     */
    public function setURL( $URL )
    {
        $this->URL = $URL;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormatReturn()
    {
        return $this->format_return;
    }

    /**
     * @param mixed $format_return
     */
    public function setFormatReturn( $format_return )
    {
        switch ( $format_return ) {
            case"json":
                $format = 'application/json';
                break;
            case"xml":
                $format = 'application/xml';
                break;
        }
        $this->format_return = $format;
        return $this;
    }


    public function __construct( $username, $password )
    {
        $this->setUsername( $username );
        $this->setPassword( $password );
    }


    public function run()
    {
        $ch = curl_init();
        // Autenticação CURLAUTH_ANY é um alias para CURLAUTH_BASIC | CURLAUTH_DIGEST | CURLAUTH_GSSNEGOTIATE | CURLAUTH_NTLM.
        curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY );
        // Usuário e Senha
        curl_setopt( $ch, CURLOPT_USERPWD, $this->getUsername() . ":" . $this->getPassword() );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $this->getMethodRequest() );
        curl_setopt( $ch, CURLOPT_HEADER, FALSE );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Accept: ' . $this->getFormatReturn() . '; charset=UTF-8' ) );
        curl_setopt( $ch, CURLOPT_URL, $this->getURL() );
        curl_setopt( $ch, CURLOPT_USERAGENT, $this->getUserAgent() );
        // O número de segundos para esperar enquanto tentando se conectar. Use 0 para esperar indefinidamente.
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 5 );
        $data = curl_exec( $ch );
        curl_close( $ch );
        return $data;
    }

    public function findAll()
    {
        $result = $this->setMethodRequest( 'GET' )
            ->setURL( $this->getHttps() . $this->getHost() . ':' . $this->getPort() . '/' . $this->getVersaoApi() . '/servers/' . $this->getServer() . '/vhosts/' . $this->getVhost() . '/applications' )
            ->run();

        return $result;
    }

    public function findByOne( $application_name )
    {
        $result = $this->setMethodRequest( 'GET' )
            ->setURL( $this->getHttps() . $this->getHost() . ':' . $this->getPort() . '/' . $this->getVersaoApi() . '/servers/' . $this->getServer() . '/vhosts/' . $this->getVhost() . '/applications/' . $application_name )
            ->run();

        return $result;
    }
}

