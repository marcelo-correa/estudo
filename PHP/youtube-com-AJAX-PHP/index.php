<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="author" content="">
    <script type="text/javascript" src="youtube.js"></script>
    <link rel="stylesheet" type="text/css" href="youtube.css"/>
</head>

<body>

<div id="geral">
    <div id="top"></div>
    <div id="centro">
        <h1>Vídeo Downloader - YouTube</h1>

        <form id="form" name="form" method="post" action="youtube.php">
            <h3>Adicione o link do vídeo no campo abaixo:</h3>
            <input type="text" size="50" name="linkyoutube"/>
            <input type="button" onclick="YouTubeLink(this.form.linkyoutube.value)" value="Gerar link de download"/>
        </form>

        <div class="exemplo">http://www.youtube.com/watch?v=HXHbBWGSwsw</div>
    </div>

    <div id="videoyoutube"></div>
    <div id="copy">Copyright 2007© - Todos os direitos reservados.</div>
    <div id="baixo"></div>
</div>

</body>
</html>