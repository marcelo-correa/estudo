<?php

/**
 * Cria a class YouTube
 */
class YouTube
{
    var $video; //define a variavel video

    /**
     * O segredo todo do script está aqui.
     * Observe que todos os vídeos do youtube podem ser baixados.
     * Essa URL envia para o servidor que tem os vídeos de download.
     */
    var $linkd = "http://173.194.118.56/get_video?video_id=";

    function YouTubeDown( $v )
    {
        $this->video = $v;

        //verifica se o link do vídeo está vazio ou se não existe a ocorrência youtube.com
        if ( empty( $this->video ) || !stristr( $this->video, "youtube.com" ) ) {
            // Escreve a mensagem caso o campo de video esteja vazio ou se não encontrar a ocorrência de youtube.com
            print( 'Erro - Link do vídeo inválido' );
        } else {
            /**
             * Os vídeos do youtube tem esse formato: youtube.com/watch?v=
             * Então, vamos "explodir" a URL para pegar apenas o que está depois do v= que no caso, é o endereço do vídeo
             */
            $exp = explode( "v=", $this->video );
            // Agora, ele imprime a URL com o vídeo para baixar.
            print( '<a href="' . $this->linkd . $exp[1] . '">Download Vídeo</a>' );
        }
    }
}

//Instânciamos a classe
$video = new YouTube();

/**
 * Pegamos a função que está dentro da class e usamos o $_GET para pegar a variável GET vinda do javascript.
 * Observe que a URL vinda do javascript é: http.open("GET", "youtube.php?video="
 * Então, temos que pagar o valor da variável video=
 */
$video->YouTubeDown( $_GET["video"] );
?>