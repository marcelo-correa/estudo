function YouTubeLink( valor ) {
    //o comando GET pega o valor do link(que foi colocado no campo text do html) e manda para a variavel video.
    http.open( "GET", "youtube.php?video=" + valor, true );
    http.onreadystatechange = handleHttpResponse;
    http.send( null );
}

function handleHttpResponse() {
    //pega o id da div videoyoutube
    var result = document.getElementById( 'videoyoutube' );
    campo_select = document.forms[0].result;

    if ( http.readyState == 4 ) {
        result.innerHTML = http.responseText;
    }
}

function getHTTPObject() {
    var xmlhttp;
    if ( !xmlhttp && typeof XMLHttpRequest != 'undefined' ) {
        try {
            xmlhttp = new XMLHttpRequest();
        } catch ( e ) {
            xmlhttp = false;
        }
    }
    return xmlhttp;
}
var http = getHTTPObject();